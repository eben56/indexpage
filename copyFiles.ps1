

$Source = "C:\Users\SPICE\Desktop\powershell\*"
$Destination = "C:\Users\SPICE\Desktop\python\powershell"

$number_of_days = "-1"
$current_date = Get-Date

# check for file existence
ForEach($file in (Get-ChildItem $Source)){
    if($file.LastWriteTime -gt ($current_date).AddDays($number_of_days)){
        Copy-Item -Path $file.fullname -Destination $Destination -Verbose
    }else{
        Write-Output "Couldn't copy theses files because they are old files that have been copied already"
    }
}

